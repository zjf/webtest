function AjaxClass()  
{  
    var XmlHttp = false;  

    try {  
        XmlHttp = new XMLHttpRequest();        //FireFox专有  
    } catch(e) {  
        try {  
            XmlHttp = new ActiveXObject("MSXML2.XMLHTTP");  
        } catch(e2) { 
            try {  
                XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");  
            } catch(e3) {  
                alert("你的浏览器不支持XMLHTTP对象，请升级到IE6以上版本！");  
                XmlHttp = false;  
            }
        }
    }
  
    var me = this;
    this.Method = "POST";  
    this.Url = "";  
    this.Async = true;  
    this.Arg = "";  
    this.CallBack = function(){};  
    this.Loading = function(){};  
      
    this.Send = function()
    {  
        if (this.Url == "") {  
            return false;  
        }  

        if (!XmlHttp) {  
            return IframePost();  
        }
  
        XmlHttp.open(this.Method, this.Url, this.Async);  

        if (this.Method == "POST") {  
            XmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");  
        }  

        XmlHttp.onreadystatechange = function()  
        {
            if (XmlHttp.readyState==4) {  
                var Result = false;  
                if (XmlHttp.status == 200) {  
                    Result = XmlHttp.responseText;  
                }  
                XmlHttp = null;  
                  
                me.CallBack(Result);  
            } else {  
                me.Loading();  
            }  
        }

        if (this.Method == "POST") {  
            XmlHttp.send(this.Arg);  
        } else {  
            XmlHttp.send(null);  
        }  
    }  
      
    //Iframe方式提交  
    function IframePost()  
    {  
        var Num = 0;  
        var obj = document.createElement("iframe");  

        obj.attachEvent("onload", function(){ me.CallBack(obj.contentWindow.document.body.innerHTML); obj.removeNode() });  
        obj.attachEvent("onreadystatechange", function(){ if (Num>=5) {obj.removeNode()} });  
        obj.src = me.Url;  
        obj.style.display = 'none';  
        document.body.appendChild(obj);  
    }  
}

function getsec(str)
{
   //alert(str);
   var str1 = str.substring(1, str.length) * 1;
   var str2 = str.substring(0, 1);

   if (str2 == "s") {
        return str1*1000;
   } else if (str2 == "h") {
       return str1*60*60*1000;
   } else if (str2 == "d") {
       return str1*24*60*60*1000;
   }
}

  // JavaScript Document
var common={
    ajaxgetrequest:function(url, async, callback){
        var Ajax = new AjaxClass();         // 创建AJAX对象  

        Ajax.Method = "GET";                // 设置请求方式为POST  
        Ajax.Url = url;    // URL为default.asp  
        Ajax.Async = async;                  // 是否异步  
        Ajax.Loading = function(){          //等待函数  
            //document.write("loading...");  
        }

        Ajax.CallBack = callback;
        Ajax.Send();
    },

    ajaxpostrequest:function(url, async, data, callback){
        var Ajax = new AjaxClass();         // 创建AJAX对象  

        Ajax.Method = "POST";               // 设置请求方式为POST  
        Ajax.Url = url;            // URL为default.asp  
        Ajax.Async = async;                  // 是否异步  
        Ajax.Arg = data;               // POST的参数  
        Ajax.Loading = function(){          //等待函数  
            //document.write("loading...");  
        }
        Ajax.CallBack = callback;
        Ajax.Send();   
    },

    setcookie:function(name, value){
        var Days = 30;
        var exp = new Date();
        exp.setTime(exp.getTime() + Days*24*60*60*1000);
        var isChrome = window.navigator.userAgent.indexOf("Chrome") !== -1 

        if(isChrome) {
            localStorage.setItem(name,value);
        } else{
            document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
        }
    },

    getcookie:function(name){
        var isChrome = window.navigator.userAgent.indexOf("Chrome") !== -1 

        if(isChrome) {
            return localStorage.getItem(name);
        } else {
            var arr, reg = new RegExp("(^| )"+name+"=([^;]*)(;|$)");

            if(arr = document.cookie.match(reg))
                return (arr[2]);
            else
                return null;
        }
    }
}
